//
//  ViewController.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/7/21.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet var buttonsView: CheckoutButtonsView!
    @IBOutlet var takeOrderButtonsView: TakeOrderButtonsView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let config = CheckoutButtonsConfig.init(walletTitle: "Portafogli", drawerTitle: "Cassetto", payTitle: "Paga",preBillTitle: "Stampa", delegate: self, hasWalletButton: true, hasDrawerButton: true, currency: "€", secondaryCurrency: nil, secondaryCurrencyExchangeRate: nil)
        buttonsView.setupWithConfig(config)
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { timer in
            let randomNumber = Double.random(in: 0...200)
            if (Bool.random()){
                if (Bool.random()){
                    self.buttonsView.styleButtonFor(.bill)
                    self.buttonsView.setPayButtonTitle(randomNumber)
                }
                else{
                    self.buttonsView.styleButtonFor(.trainingBill)
                    self.buttonsView.setPayButtonTitle(randomNumber)
                }
            }
            else{
                if (Bool.random()){
                    self.buttonsView.styleButtonFor(.preBill)
                }
            }
            self.buttonsView.drawerEnabled(Bool.random())
        }
        
        let toConfig = TakeOrderButtonsConfig.init(walletTitle: "Portafogli", addCourseTitle: "Nuova uscita", sendCourseTitle: "Invia uscita", delegate: self, hasWalletButton: true, hasAddCourseButton: true)
        takeOrderButtonsView.setupWithConfig(toConfig)
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { timer in
            self.takeOrderButtonsView.sendCourseEnabled(Bool.random())
            self.takeOrderButtonsView.walletEnabled(Bool.random())
        }
    }
}

extension ViewController: CheckoutButtonsDelegate {
    func didSelectDrawer() {
        
    }
    
    func didSelectPay() {
        
    }
    
    func didSelectPayments() {
        
    }
    
}

extension ViewController: TakeOrderButtonsViewDelegate {
    func didSelectAddCourse() {
        
    }
    
    func didSelectSendCourse() {
        
    }
    
}
