//
//  CheckoutButtons.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/7/21.
//

import Foundation
import UIKit


public protocol CheckoutButtonsDelegate:AnyObject{
    func didSelectDrawer()
    func didSelectPay()
    func didSelectPayments()
}

public class CheckoutButtonsView: UIView {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var walletView: WalletView!
    @IBOutlet private var drawerView: DrawerView!
    @IBOutlet private var payView: PayView!
    
    private var config:CheckoutButtonsConfig = CheckoutButtonsConfig.defaultConfig()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.module.loadNibNamed("CheckoutButtonsView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.backgroundColor = UIColor.almostWhite
        walletView.delegate = self
        drawerView.delegate = self
        payView.delegate = self
    }
    
    public func setupWithConfig(_ c:CheckoutButtonsConfig){
        self.config = c
        walletView.setButtonTitle(self.config.walletTitle)
        drawerView.setButtonTitle(self.config.drawerTitle)
    }
    
    public func setPayButtonTitle(_ amount:Double){
        self.payView.setPayButtonTitle(amount)
        checkDelegate()
    }
    
    /**
     Disabilita sia il bottone paga che il bottone wallet
     */
    public func disablePayments(){
        self.payView.disablePayButton()
        self.walletView.setEnabled(false)
        checkDelegate()
    }
    
    
    /**
     Abilita sia il bottone paga che il bottone wallet
     */
    public func enablePayments(){
        self.payView.disablePayButton()
        if (self.config.hasWalletButton){
            self.walletView.setEnabled(true)
        }
        
        checkDelegate()
    }
    
    public func styleButtonFor(_ document:DocumentType){
        self.payView.styleButtonFor(document)
    }

    public func paymentsEnabled(_ enabled:Bool){
        if (self.config.hasWalletButton){
            self.walletView.setEnabled(enabled)
        }
        else{
            self.walletView.setEnabled(false)
        }
        checkDelegate()
    }
    
    public func drawerEnabled(_ enabled:Bool){
        if(self.config.hasDrawerButton){
            self.drawerView.setEnabled(enabled)
        }
        else {
            self.walletView.setEnabled(false)
        }
        checkDelegate()
    }
    
    private func checkDelegate(){
        if (self.config.delegate == nil){
            debugPrint("CheckoutButtonsDelegate IS NIL, YOU SURE?")
        }
    }
}

extension CheckoutButtonsView:WalletButtonDelegate {
    func didSelectPayments() {
        debugPrint("iPratico Buttons - Payments")
        self.config.delegate?.didSelectPayments()
    }
}


extension CheckoutButtonsView:PayButtonDelegate {
    func getPreBillString() -> String {
        return self.config.preBillTitle
    }
    
    func getPayString() -> String {
        return self.config.payTitle
    }
    
    func getCurrency() -> String {
        debugPrint("iPratico Buttons - Pay")
        return self.config.currency
    }
    
    func getSecondaryCurrency() -> String? {
        debugPrint("iPratico Buttons - Pay")
        return self.config.secondaryCurrency
    }
    
    func getSecondaryCurrencyexchangeRate() -> Double? {
        debugPrint("iPratico Buttons - Pay")
        return self.config.secondaryCurrencyExchangeRate
    }
    
    func didSelectPay() {
        debugPrint("iPratico Buttons - Pay")
        self.config.delegate?.didSelectPay()
    }
}

extension CheckoutButtonsView:DrawerButtonDelegate {
    func didSelectDrawer() {
        debugPrint("iPratico Buttons - Drawer")
        self.config.delegate?.didSelectDrawer()
    }
}

