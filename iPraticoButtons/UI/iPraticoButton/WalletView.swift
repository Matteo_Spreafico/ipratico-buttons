//
//  PaymentsView.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/9/21.
//

import Foundation
import UIKit

protocol WalletButtonDelegate:AnyObject{
    func didSelectPayments()
}


/**
    Vista che gestisce il bottone "Portafogli"
 */
public class WalletView:UIView {
    private var paymentsText:String = "Wallet"
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var button: iPraticoButton!
    
    weak var delegate:WalletButtonDelegate? = nil
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        button.setup(.wallet)
        self.setEnabled(true)
        button.addTarget(self, action: #selector(pressed), for: .touchUpInside)
    }
    
    @objc func pressed() {
        self.delegate?.didSelectPayments()
    }
    
    public func setEnabled(_ enabled:Bool){
        button.isEnabled = enabled
        let image = UIImage.init(named: enabled ? "glyphsIconOrderPay" : "glyphsIconOrderPayDisabled")
        iconImageView.image = image
    }
    
    public func setButtonTitle(_ title:String){
        button.setTitle(title, for: .normal)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
}
