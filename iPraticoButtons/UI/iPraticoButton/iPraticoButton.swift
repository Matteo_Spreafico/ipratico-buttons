//
//  iPraticoButton.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/7/21.
//

import Foundation
import UIKit

public protocol iPraticoButtonProtocol where Self:iPraticoButton{
    var buttonStyle: iPraticoButton.ButtonStyle { get set }
    var bgColor: UIColor {get set}
    var buttonBorderColor: UIColor {get set}
    var buttonShadowColor: UIColor {get set}
    var buttonBorderWidth:CGFloat {get set}
}

public class iPraticoButton : UIButton, iPraticoButtonProtocol {
    public var buttonStyle:ButtonStyle = .unknown
    public var bgColor = UIColor.red
    public var buttonBorderColor = UIColor.red
    public var buttonShadowColor = UIColor.red
    public var buttonBorderWidth:CGFloat = 1.0
    
    public enum ButtonStyle:String {
        case drawer,addCourse,sendCourse, wallet, bill, trainingBill, preBill, unknown
    }
    
    private var shadowLayer: CAShapeLayer!
    
    
    public func defineColors(bgColor:UIColor?, borderColor:UIColor?, shadowColor:UIColor?, tintColor:UIColor?, borderWidth:CGFloat){
        self.bgColor = bgColor ?? UIColor.red
        self.setTitleColor(self.tintColor, for: .normal)
        self.buttonBorderColor = borderColor ?? UIColor.red
        self.buttonShadowColor = shadowColor ?? UIColor.red
        self.tintColor = tintColor ?? UIColor.red
        self.buttonBorderWidth = borderWidth
        if (shadowLayer != nil){
            shadowLayer.fillColor = self.bgColor.cgColor
            shadowLayer.borderColor = self.buttonBorderColor.cgColor
            shadowLayer.borderWidth = self.buttonBorderWidth
            shadowLayer.shadowColor = self.buttonShadowColor.cgColor
        }
        self.setNeedsLayout()
        self.setTitleColor(self.tintColor, for: .normal)
        self.setTitleColor(UIColor.smokeLight, for: .disabled)
        
        switch self.buttonStyle {
        case .drawer, .wallet, .addCourse:
            self.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 16, bottom: 0, right: 0)
            self.titleLabel?.font =  UIFont.systemFont(ofSize: 14, weight: .semibold)
        case .bill,.trainingBill,.preBill, .sendCourse:
            self.titleLabel?.font =  UIFont.systemFont(ofSize: 20, weight: .semibold)
        case .unknown:
            debugPrint("unknown")
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
    }
    
    
    public func setup(_ withStyle:ButtonStyle) {
        debugPrint("Setup -> \(withStyle.rawValue)")
        
        self.buttonStyle = withStyle
        switch self.buttonStyle {
        case .drawer, .wallet, .addCourse:
            self.contentHorizontalAlignment = .left
            self.defineColors(bgColor: UIColor.iPraticoButton.smallBackground,
                              borderColor: UIColor.iPraticoButton.smallBorderBackground,
                              shadowColor: UIColor.iPraticoButton.smallShadow,
                              tintColor: UIColor.info,
                                borderWidth: 1.0)
        case .bill, .preBill:
            self.contentHorizontalAlignment = .center
            self.defineColors(bgColor: UIColor.iPraticoButton.orangeBackground,
                              borderColor: UIColor.iPraticoButton.orangeBackground,
                              shadowColor: UIColor.iPraticoButton.orangeShadow,
                              tintColor: UIColor.white,
                              borderWidth: 1.0)
        case .trainingBill:
            self.contentHorizontalAlignment = .center
            self.defineColors(bgColor: UIColor.iPraticoButton.lightBlueBackground,
                              borderColor:UIColor.iPraticoButton.lightBlueBackground,
                              shadowColor: UIColor.iPraticoButton.lightBlueShadow,
                              tintColor: UIColor.white,
                              borderWidth: 1.0)
        case .sendCourse:
            self.contentHorizontalAlignment = .center
            self.defineColors(bgColor: UIColor.iPraticoButton.blueBackground,
                              borderColor:UIColor.iPraticoButton.blueBackground,
                              shadowColor: UIColor.iPraticoButton.blueShadow,
                              tintColor: UIColor.white,
                              borderWidth: 1.0)
        case .unknown:
            debugPrint("unknown")
        }
        //self.setTitle(self.buttonStyle.rawValue, for: .normal)
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if (isEnabled){
            self.layer.borderColor = self.buttonBorderColor.cgColor
            self.layer.borderWidth = self.buttonBorderWidth
            self.layer.cornerRadius = 12
            if shadowLayer == nil {
                shadowLayer = CAShapeLayer()
                shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 12).cgPath
                shadowLayer.fillColor = bgColor.cgColor
                shadowLayer.borderColor = buttonBorderColor.cgColor
                shadowLayer.borderWidth = buttonBorderWidth
                shadowLayer.shadowColor = buttonShadowColor.cgColor
                shadowLayer.shadowPath = shadowLayer.path
                shadowLayer.shadowOffset = CGSize(width: 0, height: 2.0)
                shadowLayer.shadowOpacity = 1
                shadowLayer.shadowRadius = 0
                self.layer.insertSublayer(shadowLayer, at: 0)
            }
        }
        else{
            self.layer.borderColor = UIColor.smokeLight.cgColor
            self.layer.borderWidth = self.buttonBorderWidth
            self.backgroundColor = UIColor.white
            self.layer.cornerRadius = 12
            if shadowLayer != nil {
                shadowLayer.removeFromSuperlayer()
                shadowLayer = nil
            }
        }
    }
    
}


