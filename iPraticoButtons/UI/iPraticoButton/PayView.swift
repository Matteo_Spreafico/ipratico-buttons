//
//  PayView.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/9/21.
//

import Foundation
import UIKit    

protocol PayButtonDelegate:AnyObject{
    func didSelectPay()
    func getCurrency() -> String
    func getSecondaryCurrency() -> String?
    func getSecondaryCurrencyexchangeRate() -> Double?

    /**
     ad esempio, "Stampa"
     */
    func getPreBillString() -> String
    
    
    /**
     ad esempio, "Paga"
     */
    func getPayString() -> String
}

public class PayView:UIView {

    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var button: iPraticoButton!
    var currency:String = ""
    
    var secondaryCurrency:String?
    var exchangeRate:Double?
    
    weak var delegate:PayButtonDelegate? = nil
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        button.setup(.bill)
        button.addTarget(self, action: #selector(pressed), for: .touchUpInside)
    }
    
    public func setPayButtonTitle(_ amount:Double){
        guard let d = self.delegate else {
            button.setTitle("no delegate!", for: .normal)
            debugPrint("No delegate :(")
            return
        }
        let buttonTitle = "\(d.getPayString()) \(amount.makePrice(d.getCurrency()))"
        button.setTitle(buttonTitle, for: .normal)
        
    }
    
    
    public func styleButtonFor(_ document:DocumentType){
        switch document {
        case .bill:
            self.configureButtonForBill()
        case .trainingBill:
            self.configureButtonForTrainingBill()
        case .preBill:
            self.configureButtonForPreBill()
        }
    }
    
    private func configureButtonForBill(){
        self.enablePayButton()
        button.setup(.bill)
        iconImageView.isHidden = true
    }
    
    private func configureButtonForTrainingBill(){
        self.enablePayButton()
        button.setup(.trainingBill)
        iconImageView.isHidden = false
        let image = UIImage.init(named:"glyphsIconChange")
        iconImageView.image = image
    }
    
    private func configureButtonForPreBill(){
        self.enablePayButton()
        button.setup(.preBill)
        iconImageView.isHidden = false
        let image = UIImage.init(named:"glyphsIconPrinter")
        iconImageView.image = image
    }
    
    public func disablePayButton(){
        iconImageView.isHidden = true
        button.isEnabled = false
    }
    
    public func enablePayButton(){
        iconImageView.isHidden = true
        button.isEnabled = true
    }
    
    @objc private func pressed() {
        self.delegate?.didSelectPay()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
}
