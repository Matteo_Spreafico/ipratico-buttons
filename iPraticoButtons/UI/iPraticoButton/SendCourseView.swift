//
//  SendCourseView.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/9/21.
//

import Foundation
import UIKit

protocol SendCourseViewDelegate:AnyObject{
    func didSelectSend()
    // protocol definition goes here
}
public class SendCourseView:UIView {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var button: iPraticoButton!
    
    weak var delegate:SendCourseViewDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        button.setup(.sendCourse)
        self.setEnabled(true)
        button.addTarget(self, action: #selector(pressed), for: .touchUpInside)
    }
    
    
    
    public func setButtonTitle(_ title:String){
        button.setTitle(title, for: .normal)
    }
    
    public func setEnabled(_ enabled:Bool){
        button.isEnabled = enabled
        let image = UIImage.init(named: enabled ? "glyphsIconOrderSend03" : "glyphsIconOrderSend03Disabled")
        iconImageView.image = image
    }
    
    @objc private func pressed() {
        self.delegate?.didSelectSend()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
}
