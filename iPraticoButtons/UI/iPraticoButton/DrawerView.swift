//
//  DrawerView.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/9/21.
//

import Foundation
import UIKit

protocol DrawerButtonDelegate:AnyObject{
    func didSelectDrawer()
    // protocol definition goes here
}
public class DrawerView:UIView {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var button: iPraticoButton!
    
    weak var delegate:DrawerButtonDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        button.setup(.drawer)
        self.setEnabled(true)
        button.addTarget(self, action: #selector(pressed), for: .touchUpInside)
    }
    
    public func setEnabled(_ enabled:Bool){
        button.isEnabled = enabled
        //iconImageView.alpha = enabled ? 1 : 0.5
        let image = UIImage.init(named: enabled ? "glyphsIconOrderSent01" : "glyphsIconOrderSent01Disabled")
        iconImageView.image = image
        
        
    }
    
    public func setButtonTitle(_ title:String){
        button.setTitle(title, for: .normal)
    }
    
    @objc func pressed() {
        self.delegate?.didSelectDrawer()
    }
    
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
}
