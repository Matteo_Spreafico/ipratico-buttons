//
//  CourseView.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/9/21.
//

import Foundation
import UIKit

protocol AddCourseViewDelegate:AnyObject{
    func didSelectAddCourse()
    // protocol definition goes here
}
public class AddCourseView:UIView {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var button: iPraticoButton!
    
    weak var delegate:AddCourseViewDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        button.setup(.addCourse)
        self.setEnabled(true)
        button.addTarget(self, action: #selector(pressed), for: .touchUpInside)
    }
    
    public func setButtonTitle(_ title:String){
        button.setTitle(title, for: .normal)
    }
    
    public func setEnabled(_ enabled:Bool){
        button.isEnabled = enabled
        let image = UIImage.init(named: enabled ? "glyphsIconPlusPlain" : "glyphsIconPlusPlainDisabled")
        iconImageView.image = image
    }
    
    @objc private func pressed() {
        self.delegate?.didSelectAddCourse()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
}
