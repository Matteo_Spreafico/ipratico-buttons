//
//  TakeOrderButtonsView.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/9/21.
//

import Foundation
import UIKit

public protocol TakeOrderButtonsViewDelegate:AnyObject{
    func didSelectAddCourse()
    func didSelectSendCourse()
    func didSelectPayments()
}

public class TakeOrderButtonsView: UIView {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var walletView: WalletView!
    @IBOutlet private var addCourseView: AddCourseView!
    @IBOutlet private var sendCourseView: SendCourseView!
    

    private var config:TakeOrderButtonsConfig = TakeOrderButtonsConfig.defaultConfig()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        guard Bundle.module.path(forResource: "TakeOrderButtonsView", ofType: "nib") != nil else {
            debugPrint("TakeOrderButtonsView is not ready for current device.")
            return
        }
        Bundle.module.loadNibNamed("TakeOrderButtonsView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.backgroundColor = UIColor.almostWhite
        walletView.delegate = self
        addCourseView.delegate = self
        sendCourseView.delegate = self
    }
    
    
    public func setupWithConfig(_ c:TakeOrderButtonsConfig){
        self.config = c
        walletView.setButtonTitle(self.config.walletTitle)
        addCourseView.setButtonTitle(self.config.addCourseTitle)
        self.setSendCourseTitle(self.config.sendCourseTitle)
    }
    
    public func setSendCourseTitle(_ title:String){
        sendCourseView.setButtonTitle(self.config.sendCourseTitle)
    }
    
    
    
    public func sendCourseEnabled(_ enabled:Bool){
        self.sendCourseView.setEnabled(enabled)
        checkDelegate()
    }
    
    
    public func walletEnabled(_ enabled:Bool){
        self.walletView.setEnabled(enabled)
        checkDelegate()
    }
    
    public func addCourseEnabled(_ enabled:Bool){
        self.addCourseView.setEnabled(enabled)
        checkDelegate()
    }
    
    private func checkDelegate(){
        if (self.config.delegate == nil){
            debugPrint("TakeOrderButtonsViewDelegate IS NIL, YOU SURE?")
        }
    }
}


extension TakeOrderButtonsView:WalletButtonDelegate {
    func didSelectPayments() {
        debugPrint("iPratico Buttons - Payments")
        self.config.delegate?.didSelectPayments()
    }
}

extension TakeOrderButtonsView:AddCourseViewDelegate {
    func didSelectAddCourse() {
        debugPrint("iPratico Buttons - Payments")
        self.config.delegate?.didSelectAddCourse()
    }
}

extension TakeOrderButtonsView:SendCourseViewDelegate {
    func didSelectSend() {
        debugPrint("iPratico Buttons - Send Course")
        self.config.delegate?.didSelectSendCourse()
    }
}
