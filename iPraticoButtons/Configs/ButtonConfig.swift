//
//  ButtonConfig.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/7/21.
//

import Foundation
import UIKit

struct CONST {
    static var CONFIG_MISSING = "Missing config"
}

public struct CheckoutButtonsConfig{

    
    public var walletTitle:String
    public var drawerTitle:String
    public var payTitle:String
    public var preBillTitle:String
    public var delegate:CheckoutButtonsDelegate?
    public var hasWalletButton:Bool
    public var hasDrawerButton:Bool
    public var currency:String
    public var secondaryCurrency:String?
    public var secondaryCurrencyExchangeRate:Double?
    
    public init(walletTitle: String, drawerTitle: String, payTitle: String, preBillTitle: String, delegate: CheckoutButtonsDelegate? = nil, hasWalletButton: Bool, hasDrawerButton: Bool, currency: String, secondaryCurrency: String? = nil, secondaryCurrencyExchangeRate: Double? = nil) {
        self.walletTitle = walletTitle
        self.drawerTitle = drawerTitle
        self.payTitle = payTitle
        self.preBillTitle = preBillTitle
        self.delegate = delegate
        self.hasWalletButton = hasWalletButton
        self.hasDrawerButton = hasDrawerButton
        self.currency = currency
        self.secondaryCurrency = secondaryCurrency
        self.secondaryCurrencyExchangeRate = secondaryCurrencyExchangeRate
    }
    
    static func defaultConfig() -> CheckoutButtonsConfig{
        CheckoutButtonsConfig.init(walletTitle: CONST.CONFIG_MISSING, drawerTitle: CONST.CONFIG_MISSING, payTitle: CONST.CONFIG_MISSING,preBillTitle: CONST.CONFIG_MISSING, delegate: nil, hasWalletButton: true, hasDrawerButton: true, currency: "£", secondaryCurrency: nil, secondaryCurrencyExchangeRate:nil)
    }
}

public enum DocumentType {
    case bill, preBill, trainingBill
}


public struct TakeOrderButtonsConfig{
    
    public var walletTitle:String
    public var addCourseTitle:String
    public var sendCourseTitle:String
    public var delegate:TakeOrderButtonsViewDelegate?
    public var hasWalletButton:Bool
    public var hasAddCourseButton:Bool

    public init(walletTitle: String, addCourseTitle: String, sendCourseTitle: String, delegate: TakeOrderButtonsViewDelegate? = nil, hasWalletButton: Bool, hasAddCourseButton: Bool) {
        self.walletTitle = walletTitle
        self.addCourseTitle = addCourseTitle
        self.sendCourseTitle = sendCourseTitle
        self.delegate = delegate
        self.hasWalletButton = hasWalletButton
        self.hasAddCourseButton = hasAddCourseButton
    }
    
    static func defaultConfig() -> TakeOrderButtonsConfig{
        return TakeOrderButtonsConfig.init(walletTitle: CONST.CONFIG_MISSING,addCourseTitle: CONST.CONFIG_MISSING,sendCourseTitle: CONST.CONFIG_MISSING, delegate: nil, hasWalletButton: true, hasAddCourseButton: true)
    }
}
