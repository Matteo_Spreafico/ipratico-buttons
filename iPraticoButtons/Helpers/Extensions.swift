//
//  Extensions.swift
//  iPraticoButtons
//
//  Created by Matteo Spreafico on 12/9/21.
//

import Foundation
import UIKit

internal extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    var twoDecimalString: String {
        return String(format: "%.2f", (self*100)/100)
    }
    var threeDecimalString: String {
        return String(format: "%.3f", (self*1000)/1000)
    }
    var cleanDecimal: String {
        let round = (self*1000)/1000
        return round.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", round) : String(round)
    }
    var twoDigits:Double {
        return (self*100).rounded()/100
    }
    
    var makePrice: String{
        if (self >= 0){
            return "€" + " " + self.twoDecimalString
        }
        else{
            let absDouble = self * -1
            return "- " + "€" + " " + absDouble.twoDecimalString
        }
    }
    
    func makePrice(_ currency:String) -> String{

        if (self >= 0){
            return currency + " " + self.twoDecimalString
        }
        else{
            let absDouble = self * -1
            return "- " + currency + " " + absDouble.twoDecimalString
        }
    }
}


internal extension UIColor{
    struct iPraticoButton {
        public static let smallBackground = UIColor(red: 245.0 / 255.0, green: 248 / 255.0, blue: 255 / 255.0, alpha: 1.0)
        public static let smallBorderBackground = UIColor(red:  222 / 255.0, green: 232.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
        public static let smallShadow = UIColor(red:  182 / 255.0, green: 196 / 255.0, blue: 227 / 255.0, alpha: 1.0)
        
        public static let orangeBackground = UIColor(red: 247.0 / 255.0, green: 147 / 255.0, blue: 26 / 255.0, alpha: 1.0)
        public static let orangeBorderBackground = UIColor(red: 247.0 / 255.0, green: 147 / 255.0, blue: 26 / 255.0, alpha: 1.0)
        public static let orangeShadow = UIColor(red: 183 / 255.0, green: 92 / 255.0, blue: 12 / 255.0, alpha: 1.0)
        
        public static let lightBlueBackground = UIColor(red: 80.0 / 255.0, green: 152 / 255.0, blue: 230 / 255.0, alpha: 1.0)
        public static let lightBlueBorderBackground = UIColor(red: 80.0 / 255.0, green: 152 / 255.0, blue: 230 / 255.0, alpha: 1.0)
        public static let lightBlueShadow = UIColor(red: 45.0 / 255.0, green: 109 / 255.0, blue: 178 / 255.0, alpha: 1.0)
        
        public static let blueBackground = UIColor(red: 0 / 255.0, green: 113 / 255.0, blue: 242 / 255.0, alpha: 1.0)
        public static let blueBorderBackground = UIColor(red: 0 / 255.0, green: 113 / 255.0, blue: 242 / 255.0, alpha: 1.0)
        public static let blueShadow = UIColor(red: 0 / 255.0, green: 84 / 255.0, blue: 152 / 255.0, alpha: 1.0)
    }
    static let info = UIColor(red: 0.0, green: 113.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
    static let smokeLight = UIColor(red:  217.0 / 255.0, green: 217.0 / 255.0, blue: 217.0 / 255.0, alpha: 1.0)
    static let almostWhite = UIColor(red:  249 / 255.0, green: 249 / 255.0, blue: 249 / 255.0, alpha: 1.0)
}
